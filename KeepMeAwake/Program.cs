using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Runtime.InteropServices;

public class KeepMeAwakeService : BackgroundService
{
    const uint ES_CONTINUOUS = 0x80000000;
    const uint ES_SYSTEM_REQUIRED = 0x00000001;

    [DllImport("kernel32.dll", SetLastError = true)]
    static extern uint SetThreadExecutionState(uint esFlags);

    private int sleepTimeMinutes = 1; // Default sleep time

    public static async Task Main(string[] args)
    {
        await CreateHostBuilder(args).Build().RunAsync();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureServices((hostContext, services) =>
            {
                services.AddHostedService<KeepMeAwakeService>();
            });

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            // Prevent system from entering sleep
            SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED);

            // Sleep for the specified time
            await Task.Delay(TimeSpan.FromMinutes(sleepTimeMinutes), stoppingToken);
        }
    }
}
